import fs from 'fs';
import path from 'path';
import { unified } from 'unified';
import remarkParse from 'remark-parse';
import remarkFrontmatter from 'remark-frontmatter';
import extractFrontmatter from 'remark-extract-frontmatter';
import remarkStringify from 'remark-stringify';
import yaml from 'yaml';
import { visit } from 'unist-util-visit';
import crypto from 'crypto';

export interface ArticleData {
	title?: string;
	publisher_id?: string;
	[key: string]: string|undefined;
}

export interface CodeBlockMeta {
	gist?: boolean;
	[key: string]: any;
}

interface ProcessMarkownOptions {
	onData?: (data: ArticleData) => void;
	onCode?: (node: any, meta: CodeBlockMeta, gistFilename?: string) => void;
	onLocalImage?: (node: any, gistFilename?: string) => void;
}

export class Article {
	protected constructor(
		public readonly data: ArticleData,
		public readonly markdown: string,
		public readonly filename?: string,
	) {}

	async processMarkdown(options: ProcessMarkownOptions) {
		const file = await unified()
			.use(remarkParse)
			.use(remarkFrontmatter)
			.use(remarkStringify)
			.use(() => async tree => {
				if (tree.children?.[0]?.type !== 'yaml') {
					tree.children?.unshift({
						type: 'yaml',
						value: ''
					});
				}

				visit(tree, "yaml", (node: any) => {
					const data = yaml.parse(node.value) || {};

					options.onData && options.onData(data);

					if (data.cover_image && !data.cover_image.match(/^https?:/)) {
						const coverImageNode = {
							type: "image",
							url: data.cover_image
						};

						const gistFilename = this.getImageGistFilename(coverImageNode.url);
						options.onLocalImage && options.onLocalImage(coverImageNode, gistFilename);

						data.cover_image = coverImageNode.url;
					}

					node.value = yaml.stringify(data);
				})

				visit(tree, "code", (node: any) => {
					const meta = Article.parseCodeMeta(node.meta);

					const gistHash = crypto.createHash('md5').update(node.value.replace(/\s+/g, '')).digest('hex');
					const gistFilename = `${gistHash}.${node.lang}`;

					options.onCode && options.onCode(node, meta, gistFilename);
				})

				visit(tree, "image", (node: any) => {
					if (!node.url.match(/^https?:/)) {
						const gistFilename = this.getImageGistFilename(node.url);
						options.onLocalImage && options.onLocalImage(node, gistFilename);
					}
				})
			})
			.process(this.markdown);

		return file.toString();
	}

	protected getImageGistFilename(filepath: string) {
		if (!this.filename) {
			return undefined;
		}

		const data = fs.readFileSync(path.join(path.dirname(this.filename), filepath));
		const gistHash = crypto.createHash('md5').update(data).digest('hex');
		const gistFilename = `${gistHash}${path.extname(filepath)}`;

		return gistFilename;
	}

	static async fromFile(filename: string) {
		const markdown = fs.readFileSync(filename, 'utf-8');
		const data = await this.parseFrontmatter(markdown);

		return new Article(data, markdown, filename);
	}

	static async fromMarkdown(markdown: string) {
		const data = await this.parseFrontmatter(markdown);

		return new Article(data, markdown);
	}

	static async parseFrontmatter(markdown: string) {
		const file = await unified()
			.use(remarkParse)
			.use(remarkFrontmatter)
			.use(extractFrontmatter, {yaml: yaml.parse})
			.use(remarkStringify)
			.process(markdown);

		return file.data as ArticleData;
	}

	static parseCodeMeta(str: string|undefined) {
		const entries = str?.match(/(?:[^\s"']+|("|')[^"']*("|'))+/g)
			?.map(it => it.split('='))
			.map(([key, value]) => [key, value ? value.replace(/(^[\s"']+|[\s"']+$)/g, '') : true]) || [];

		return Object.fromEntries(entries) as CodeBlockMeta;
	}
}
