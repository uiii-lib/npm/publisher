import axios, { AxiosInstance } from "axios";

import { Article } from '../article.js';
import { DevPublishConfig } from "../config.js";
import { Gist } from '../gist.js';
import { Adapter } from '../model/adapter.js';

export class DevAdapter implements Adapter {
	readonly baseUrl = 'https://dev.to/api';

	protected client: AxiosInstance;

	constructor() {
		if (!process.env.DEV_API_KEY) {
			throw new Error('Missing env variable DEV_API_KEY with Dev.to API key');
		}

		this.client = axios.create({
			baseURL: this.baseUrl,
			headers: {
				'api-key': process.env.DEV_API_KEY
			}
		});
	}

	async register(articleUrl: string, publisherId: string) {
		const url = new URL(articleUrl);
		const devArticle = await this.findDevArticle(it => it.path === url.pathname);

		if (!devArticle) {
			throw new Error('Article not found.');
		}

		const article = await Article.fromMarkdown(devArticle.body_markdown);
		const markdown = await article.processMarkdown({
			onData: (data) => {
				data.publisher_id = publisherId;
			}
		});

		await this.client.put(`articles/${devArticle.id}`, {
			article: {
				body_markdown: markdown
			}
		});

			console.log(`Dev.to: article "${devArticle.title}" sucessfuly registered for publishing`)
	}

	async publish(article: Article, gist: Gist, config: DevPublishConfig) {
		const markdown = await article.processMarkdown({
			onCode: (node, meta, gistFilename) => {
				if (meta.gist && config.embedGistCode && gistFilename) {
					node.type = 'text';
					node.value = `{% gist https://gist.github.com/${gist.data.owner.login}/${gist.data.id} file=${gistFilename} %}`;
				} else {
					node.meta = null;
				}
			},
			onLocalImage: (node, gistFilename) => {
				if (gistFilename) {
					node.url = gist.data.files[gistFilename].raw_url
				}
			}
		});

		const devArticle = await this.findDevArticle(async it => {
			const data = await Article.parseFrontmatter(it.body_markdown);
			return data.publisher_id === article.data.publisher_id;
		});

		let response;

		if (devArticle) {
			console.log(`Dev.to: updating existing ${devArticle.published ? 'published' : 'draft'} article "${devArticle.title}" [${devArticle.id}]`)
			response = await this.client.put(`articles/${devArticle.id}`, {
				article: {
					body_markdown: markdown
				}
			});

		} else {
			console.log(`Dev.to: creating new article "${article.data.title}"`)
			response = await this.client.post('articles', {
				article: {
					body_markdown: markdown
				}
			});
		}
	}

	protected async findDevArticle(predicate: (article: any) => boolean|Promise<boolean>) {
		let page = 1;

		while (true) {
			const {data: articles} = await this.client.get('articles/me/all', {
				params: {
					page,
					per_page: 2
				}
			});

			if (articles.length === 0) {
				break;
			}

			for (let article of articles) {
				if (await predicate(article)) {
					return article;
				}
			}

			++page;
		}
	}
}
