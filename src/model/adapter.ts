import { Article } from "../article.js";
import { Gist } from "../gist.js";

export interface Adapter {
	publish(article: Article, gist: Gist, config: any): Promise<void>;
}
