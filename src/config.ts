import fs from 'fs';
import yaml from 'yaml';

export interface DevPublishConfig {
	embedGistCode?: boolean;
}

export class Config {
	files: string[];
	publish: {
		dev?: DevPublishConfig,
		[key: string]: any
	};

	constructor(filepath: string) {
		const config = yaml.parse(fs.readFileSync(filepath, 'utf-8'));

		this.files = config.files;
		this.publish = {};

		for (let key of Object.keys(config.publish)) {
			this.publish[key] = config.publish[key] || {};
		}
	}
}
