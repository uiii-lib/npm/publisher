import { Command } from 'commander';

import { DevAdapter } from '../adapters/dev.adapter.js';

const adapters: {[key: string]: any} = {
	dev: DevAdapter
};

export class RegisterCommand {
	static register(program: Command) {
		program.command('register')
			.description('register existing article for publishing')
			.argument('<platform>', `name of platform adapter: ${Object.keys(adapters).join(', ')}`)
			.argument('<article-url>', 'article URL')
			.argument('<publisher-id>', 'publisher ID to set')
			.action(RegisterCommand.run);
	}

	static async run(adapterKey: string, articleUrl: string, publisherId: string) {
		const Adapter = adapters[adapterKey];
		const adapter = new Adapter();
		await adapter.register(articleUrl, publisherId);
	}
}
