import fs from 'fs';
import path from 'path';

import { Article } from "../article.js";
import { Config } from '../config.js';
import { Gist } from "../gist.js";

import { DevAdapter } from '../adapters/dev.adapter.js';
import { Command } from 'commander';

const adapters: {[key: string]: any} = {
	dev: DevAdapter
};

export interface PublishOptions {
	config: string;
}

export class PublishCommand {
	static register(program: Command) {
		program.command('publish')
			.description('publish articles')
			.option('-c, --config <config-file>', 'path to config YAML file', 'publisher.config.yml')
			.action(PublishCommand.run);
	}

	static async run(options: PublishOptions) {
		const config = new Config(options.config);

		for (let file of config.files) {
			const filepath = path.join(process.cwd(), path.dirname(options.config), file);
			const article = await Article.fromFile(filepath);
			await PublishCommand.publish(article, config);
		}
	}

	static async publish(article: Article, config: Config) {
		for (let field of ['title', 'publisher_id']) {
			if (!article.data[field]) {
				throw new Error(`You have to specify '${field}' in article's markdown front matter`);
			}
		}

		const gist = new Gist(article.data.publisher_id!);
		await gist.load();

		await article.processMarkdown({
			onCode: (node, meta, gistFilename) => {
				if (meta.gist && gistFilename) {
					gist.addFile(gistFilename, node.value);
				}
			},
			onLocalImage: (node, gistFilename) => {
				if (article.filename && gistFilename) {
					const data = fs.readFileSync(path.join(path.dirname(article.filename), node.url));
					gist.addFile(gistFilename, data);
				}
			}
		});

		await gist.save(article);

		for (let adapterKey of Object.keys(config.publish)) {
			const Adapter = adapters[adapterKey];
			const adapter = new Adapter();
			await adapter.publish(article, gist, config.publish[adapterKey]);
		}

		gist.clean();
	}
}
