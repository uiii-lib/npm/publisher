import fs from 'fs';
import path from 'path';
import git from 'isomorphic-git';
import http from 'isomorphic-git/http/node/index.js';
import axios from 'axios';
import rimraf from 'rimraf';

import { Octokit } from "@octokit/core";
import { Article } from './article';

export class Gist {
	data: any;

	protected octokit: Octokit;

	protected dir: string = '.gist';

	constructor(protected publisherId: string) {
		if (!process.env.GIST_ACCESS_TOKEN) {
			throw new Error('Missing env variable GIST_ACCESS_TOKEN with GitHub personal access token');
		}

		this.octokit = new Octokit({auth: process.env.GIST_ACCESS_TOKEN});
	}

	addFile(filename: string, content: any) {
		fs.writeFileSync(path.join(this.dir, filename), content);
	}

	async load() {
		this.data = await this.findGist(this.publisherId);

		if (!this.data) {
			const response = await this.octokit.request('POST /gists', {
				files: {
					[`publisher_id`]: {
						content: this.publisherId
					}
				},
				public: false
			});

			this.data = response.data;
		}

		await git.clone({
			fs,
			http,
			dir: this.dir,
			url: this.data.git_pull_url,
			onAuth: () => ({
				username: this.data.owner.login,
				password: process.env.GIST_ACCESS_TOKEN
			})
		});
	}

	async save(article: Article) {
		for (let filename of fs.readdirSync(this.dir)) {
			if (filename.startsWith('.')) continue;

			await git.add({
				fs,
				dir: this.dir,
				filepath: filename
			});
		}

		await git.commit({
			fs,
			dir: this.dir,
			author: {
				name: 'publisher'
			},
			message: 'update'
		});

		await git.push({
			fs,
			dir: this.dir,
			http,
			onAuth: () => ({
				username: this.data.owner.login,
				password: process.env.GIST_ACCESS_TOKEN
			})
		});

		const response = await this.octokit.request(`PATCH /gists/${this.data.id}`, {
			description: `Code snippets and images referenced by article '${article.data.title}'`,
			public: false
		});

		this.data = response.data;
	}

	async clean() {
		rimraf.sync(this.dir);
	}

	protected async findGist(publisherId: string) {
		let page = 0;

		while (true) {
			const response = await this.octokit.request('GET /gists', {
				page
			});

			if (response.data.length === 0) {
				break;
			}

			const gists = response.data;
			for (let gist of gists) {
				const publisherIdFile = gist.files['publisher_id'];

				if (publisherIdFile) {
					const {data: publisherIdFileContent} = await axios.get(publisherIdFile.raw_url!);
					if (publisherIdFileContent === publisherId) {
						return gist;
					}
				}
			}

			++page;
		}
	}
}
