# Publisher

Tool for publishing markdown articles to multiple platforms.

## Install

```
npm install --save @uiii-lib/publisher
```

## Config

Set auth environment variables (you can use `.env` file):
- `GIST_ACCESS_TOKEN` - GitHub personal access token with `gist` scope
- `DEV_API_KEY` - Dev.to API key

Create a file `publisher.config.yml`

```yaml
files:
  - article-1.md
  - article-2.md
publish:
  dev:
    embedGistCode: true
```

## Run

```
$ .\node_modules\.bin\publisher
```
