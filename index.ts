#!/usr/bin/env node

import dotenv from 'dotenv';
import { program } from 'commander';

import { PublishCommand } from './src/commands/publish.command.js';
import { RegisterCommand } from './src/commands/register.command.js';

async function main() {
	dotenv.config();

	program.name('publisher');

	PublishCommand.register(program);
	RegisterCommand.register(program);

	await program.parseAsync();
}

main();
